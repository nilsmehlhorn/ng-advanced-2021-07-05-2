import { RouterState } from '@angular/router';
import { routerReducer } from '@ngrx/router-store';
import { ActionReducerMap } from '@ngrx/store';

export interface RootState {
  router: RouterState;
}

export const reducers: ActionReducerMap<RootState> = {
  router: routerReducer
};
