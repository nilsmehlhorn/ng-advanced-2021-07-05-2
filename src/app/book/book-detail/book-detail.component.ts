import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { Book } from '../models';
import { deleteBook } from '../store/book-collection.actions';
import { activeBook } from '../store/book-collection.selectors';

@Component({
  selector: 'ws-book-detail',
  styleUrls: ['./book-detail.component.scss'],
  templateUrl: 'book-detail.component.html'
})
export class BookDetailComponent {
  public book$: Observable<Book>;

  constructor(private store: Store) {
    this.book$ = this.store
      .select(activeBook)
      .pipe(filter((bookOrUndefined): bookOrUndefined is Book => !!bookOrUndefined));
  }

  remove() {
    this.book$.pipe(first()).subscribe(book => this.store.dispatch(deleteBook({ book })));
  }
}
