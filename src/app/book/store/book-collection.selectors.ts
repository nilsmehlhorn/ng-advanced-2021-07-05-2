import { createSelector } from '@ngrx/store';
import { selectRouteParam } from '../../store/router/router.selectors';
import { adapter } from './book-collection.reducer';
import { bookFeature } from './book.feature';

export const bookCollection = createSelector(bookFeature, feature => feature.bookCollection);

export const { selectEntities, selectAll } = adapter.getSelectors(bookCollection);

export const bookByIsbn = (isbn: string) => createSelector(selectEntities, books => books[isbn]);

export const activeBook = createSelector(selectRouteParam('isbn'), selectEntities, (isbn, books) => {
  if (isbn) {
    return books[isbn];
  }
  return undefined;
});
