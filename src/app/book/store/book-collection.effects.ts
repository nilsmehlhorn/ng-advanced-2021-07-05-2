import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of, timer } from 'rxjs';
import { tap, map, mergeMap, switchMap, concatMap, catchError } from 'rxjs/operators';
import { BookApiService } from '../book-api.service';
import {
  createBook,
  createBookComplete,
  deleteBook,
  deleteBookComplete,
  deleteBookFailed,
  loadBooks,
  loadBooksComplete
} from './book-collection.actions';

@Injectable()
export class BookCollectionEffects {
  load$ = createEffect(() =>
    this.action$.pipe(
      ofType(loadBooks),
      switchMap(() => this.bookApi.getAll()),
      map(books => loadBooksComplete({ books }))
    )
  );

  create$ = createEffect(() =>
    this.action$.pipe(
      ofType(createBook),
      mergeMap(({ book }) => this.bookApi.create(book)),
      map(book => createBookComplete({ book }))
    )
  );

  navigate$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(createBookComplete, deleteBook),
        tap(() => this.router.navigateByUrl('/'))
      ),
    {
      dispatch: false
    }
  );

  delete$ = createEffect(() =>
    this.action$.pipe(
      ofType(deleteBook),
      concatMap(action =>
        timer(2000).pipe(
          concatMap(() => this.bookApi.delete(action.book.isbn)),
          map(book => deleteBookComplete({ book })),
          catchError(err => {
            console.error(err);
            return of(deleteBookFailed({ book: action.book }));
          })
        )
      )
    )
  );

  constructor(private action$: Actions, private bookApi: BookApiService, private router: Router) {}
}
