import { createAction, props } from '@ngrx/store';
import { Book } from '../models';

export const createBook = createAction('[Book] Create', props<{ book: Book }>());
export const createBookComplete = createAction('[Book] Create Complete', props<{ book: Book }>());

export const loadBooks = createAction('[Book] Load');
export const loadBooksComplete = createAction('[Book] Load Complete', props<{ books: Book[] }>());

export const deleteBook = createAction('[Book] Delete', props<{ book: Book }>());
export const deleteBookComplete = createAction('[Book] Delete Complete', props<{ book: Book }>());
export const deleteBookFailed = createAction('[Book] Delete Failed', props<{ book: Book }>());
