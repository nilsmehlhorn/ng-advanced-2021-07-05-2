import { createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Book } from '../models';
import { createBookComplete, deleteBook, deleteBookFailed, loadBooksComplete } from './book-collection.actions';
import { BookCollectionSlice } from './book-collection.slice';

export const adapter = createEntityAdapter<Book>({
  selectId: book => book.isbn
});

const initialData: BookCollectionSlice = adapter.getInitialState();

export const bookCollectionReducer = createReducer(
  initialData,
  on(createBookComplete, deleteBookFailed, (state, action) => {
    return adapter.addOne(action.book, state);
  }),
  on(loadBooksComplete, (state, action) => adapter.setAll(action.books, state)),
  on(deleteBook, (state, action) => {
    return adapter.removeOne(action.book.isbn, state);
  })
);
