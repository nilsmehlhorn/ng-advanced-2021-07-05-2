import { BookApiService } from './book-api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { BookNa } from './models';

describe(BookApiService.name, () => {
  let httpMock: HttpTestingController;
  let bookApi: BookApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookApiService]
    });
    httpMock = TestBed.inject(HttpTestingController);
    bookApi = TestBed.inject(BookApiService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should provide books [callback variant]', done => {
    const books = [new BookNa(), new BookNa()]; // 1
    // 2
    bookApi.getAll().subscribe(booksFromApi => {
      expect(booksFromApi).toEqual(books); // 4
      done();
    });
    httpMock.expectOne('http://localhost:4730/books').flush(books); // 3
  });

  it('should provide books [promise variant]', async () => {
    const books = [new BookNa(), new BookNa()];
    const books$ = bookApi.getAll().toPromise();
    httpMock.expectOne('http://localhost:4730/books').flush(books);
    await expectAsync(books$).toBeResolvedTo(books);
  });

  it('should throw network error', done => {
    bookApi.getAll().subscribe(fail, error => {
      expect(error).toEqual(new Error('Sorry, network error'));
      done();
    });
    httpMock.expectOne('http://localhost:4730/books').error(new ErrorEvent('network error'));
  });

  it('should throw API error', async () => {
    const books$ = bookApi.getAll().toPromise();
    httpMock.expectOne('http://localhost:4730/books').flush('API Error', {
      status: 500,
      statusText: 'API broke'
    });
    // try {
    //   await books$;
    //   fail();
    // } catch (e) {
    //   expect(e).toEqual(new Error('Sorry, API Error'));
    // }
    await expectAsync(books$).toBeRejectedWith(new Error('Sorry, API error'));
  });
});
