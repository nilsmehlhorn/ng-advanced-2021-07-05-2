import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { MatInputHarness } from '@angular/material/input/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { BookApiService } from '../book-api.service';
import { BookNewComponent } from './book-new.component';

describe(BookNewComponent.name, () => {
  let bookApi: jasmine.SpyObj<BookApiService>;
  let createComponent: () => [ComponentFixture<BookNewComponent>, HarnessLoader];
  beforeEach(() => {
    bookApi = jasmine.createSpyObj<BookApiService>(['create']);
    TestBed.configureTestingModule({
      declarations: [BookNewComponent],
      imports: [
        MatButtonModule,
        NoopAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        {
          provide: BookApiService,
          useValue: bookApi
        }
      ]
    });
    createComponent = () => {
      const fixture = TestBed.createComponent(BookNewComponent);
      const loader = TestbedHarnessEnvironment.loader(fixture);
      fixture.detectChanges();
      return [fixture, loader];
    };
  });

  it('should create', () => {
    const [fixture] = createComponent();
    expect(fixture).toBeDefined();
  });

  it('should display ISBN length error', async () => {
    const [fixture, loader] = createComponent();
    const harness = await loader.getHarness(MatFormFieldHarness.with({ selector: '[data-test="isbn-field"]' }));

    const initialErrors = await harness.getTextErrors();
    expect(initialErrors).toHaveSize(0);

    const input = (await harness.getControl()) as MatInputHarness;
    await input.setValue('99');
    await input.blur();

    const errors = await harness.getTextErrors();
    expect(errors).toContain('ISBN has to be at least 3 characters long.');
  });
});
