import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory } from '@ngneat/spectator';
import { MockModule } from 'ng-mocks';
import { BookNa } from '../models';
import { BookCardComponent } from './book-card.component';

describe('BookCardComponent', () => {
  describe('unit', () => {
    it('should render n/a when no content is passed', () => {
      const component = new BookCardComponent();
      expect(component.content.abstract).toEqual('n/a');
    });
  });

  describe('template', () => {
    let fixture: ComponentFixture<BookCardComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [BookCardComponent],
        imports: [MatCardModule, RouterTestingModule]
      });
      fixture = TestBed.createComponent(BookCardComponent);
      fixture.detectChanges();
    });

    it('should render content', () => {
      fixture.componentInstance.content = {
        ...new BookNa(),
        title: 'My Great Book'
      };
      fixture.detectChanges();
      const title = fixture.debugElement.query(By.css('[data-test="card-title"]'));
      const titleElement = title.nativeElement as HTMLElement;
      expect(titleElement.innerHTML).toEqual('My Great Book');
    });
  });

  describe('spectator', () => {
    const createComponent = createComponentFactory({
      component: BookCardComponent,
      imports: [MockModule(MatCardModule), RouterTestingModule]
    });

    it('should render content', () => {
      const spectator = createComponent();
      spectator.setInput({
        content: {
          ...new BookNa(),
          title: 'My Great Book'
        }
      });
      const title = spectator.query('[data-test="card-title"]');
      expect(title).toContainText('My Great Book');
    });
  });
});
