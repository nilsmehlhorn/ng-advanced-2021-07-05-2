import { Component, Input } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { BookApiService } from '../book-api.service';
import { Book, BookNa } from '../models';
import { BookListComponent } from './book-list.component';

@Component({
  selector: 'ws-book-card',
  template: ''
})
class MockBookCardComponent {
  @Input()
  content: Book | undefined;
}

describe(BookListComponent.name, () => {
  let bookApiMock: jasmine.SpyObj<BookApiService>;

  beforeEach(() => {
    bookApiMock = jasmine.createSpyObj<BookApiService>(['getAll']);
    TestBed.configureTestingModule({
      declarations: [BookListComponent, MockBookCardComponent],
      providers: [
        {
          provide: BookApiService,
          useValue: bookApiMock
        }
      ]
    });
  });

  it('should render book cards', () => {
    bookApiMock.getAll.and.returnValue(of([new BookNa(), new BookNa()]));
    const fixture = TestBed.createComponent(BookListComponent);
    fixture.detectChanges();

    const bookCardFixtures = fixture.debugElement.queryAll(By.css('[data-test="book-list-card"]'));
    expect(bookCardFixtures).toHaveSize(2);
  });
});
