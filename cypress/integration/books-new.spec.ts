describe('New Book page', () => {
  const isbn = '123456786';

  it('should add book', () => {
    cy.visit('/');
    cy.get('[data-test="book-card"]').as('books');
    cy.get('@books').then(books => {
      cy.get('[data-test="new-book-link"]').click();
      cy.get('[data-test="isbn-input"]').type(isbn);
      cy.get('[data-test="title-input"]').type('My Great Book');
      cy.get('[data-test="author-input"]').type('Max Mustermann');
      cy.get('[data-test="create-button"]').click();
      cy.location('pathname').should('eq', '/book');
      cy.get('@books').should('have.length', books.length + 1);
    });
  });

  afterEach(() => {
    cy.request('DELETE', `http://localhost:4730/books/${isbn}`);
  });
});
