describe('Books Page', () => {
  beforeEach(() => {
    cy.intercept('http://localhost:4730/books', { fixture: 'books' });
    cy.visit('/');
  });

  it('should display books', () => {
    cy.get('[data-test="main-title"]').contains('BOOK MONKEY');
    cy.get('[data-test="book-card"]').should('have.length', 2);
  });
});
